




- 🖤  Applying computational models to biomedical research. 

- 👷 Currently working on [cytounet](https://github.com/Nelson-Gon/cytounet).


 - 💻 Simplicity focused open source advocate. Author of several open source `R` and `python` [packages](https://nelson-gon.github.io/projects).
Contributor to packages like [similiars](https://github.com/davidsjoberg/similiars).



- 📫 [Twitter](https://twitter.com/bionelsongon)  


Thank you 🖤



